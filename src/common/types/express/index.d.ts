import { IPartner } from '../../interface/PartnerInterface';

declare global {
    namespace Express {
        interface Request {
            authId: string;
            authToken: string;
            sessionId: string;
            isTFAVerified: string;
            loginType: string;
        }
    }
}
