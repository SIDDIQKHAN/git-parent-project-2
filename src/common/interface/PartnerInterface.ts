import * as t from 'io-ts';

export const PartnerToken = t.type({
    token: t.string,
});

export type PartnerTokenType = t.TypeOf<typeof PartnerToken>;
