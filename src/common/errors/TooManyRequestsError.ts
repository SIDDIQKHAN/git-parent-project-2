import { BaseError, HttpStatusCode } from './BaseError';
export class TooManyRequestsError extends BaseError {
    constructor(description = 'TooManyRequests') {
        super('TooManyRequests', HttpStatusCode.TOO_MANY_REQUESTS, description, true);
    }
}
