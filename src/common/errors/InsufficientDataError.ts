import { BaseError, HttpStatusCode } from './BaseError';
export class InsufficientDataError extends BaseError {
    constructor(description = 'Insufficient Data Error') {
        super('InsufficientDataError', HttpStatusCode.BAD_REQUEST, description, true);
    }
}
