import finicityExpress, { NextFunction, Request, Response } from 'express';
import httpStatus from 'http-status';

import * as txpushService from '@txpush-route/txpushService'

const txpushRouter = finicityExpress.Router();

export { txpushRouter };

txpushRouter.post('/create', async (req: Request, res: Response, next: NextFunction) => {
    try {
        const result = await txpushService.createEvents();
        res.status(httpStatus.CREATED).json({ message: 'Events created successfully', parentTwoResponse: result });
    } catch (err) {
        next(err);
    }
});
