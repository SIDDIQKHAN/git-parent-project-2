import { isNil } from 'lodash';

import { LoggerTraceability } from '@common/Logger';

const logger = LoggerTraceability.getInstance().getLogger();

export { reqInterceptor, reqInterceptorError, resInterceptor, resInterceptorError };

async function commonRequestInterceptor(req: any) {
    req.requestStartedAt = new Date().getTime();
    req.timeout = 60000;
    logger.info(`Calling uri ${req.method.toUpperCase()} ${req.url}`);
    return req;
}

async function commonResponseInterceptor(res: any) {
    const completionTime = new Date().getTime() - res.config.requestStartedAt;

    const status = !isNil(res.status) ? res.status : '';
    const statusText = !isNil(res.statusText) ? res.statusText : 'NA';
    logger.info(
        `Response uri ${res.config.method.toUpperCase()}  ${
            res.config.url
        } status: ${status} ${statusText} received in ${completionTime} ms`
    );
}

const reqInterceptor = (req: any) => {
    commonRequestInterceptor(req);
    return req;
};
const reqInterceptorError = (error: any) => {
    commonRequestInterceptor(error);
    return Promise.reject(error);
};
const resInterceptor = (res: any) => {
    commonResponseInterceptor(res);
    return res;
};
const resInterceptorError = (error: any) => {
    commonResponseInterceptor(error);
    return Promise.reject(error);
};
